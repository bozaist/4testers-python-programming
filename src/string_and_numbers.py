first_name1 = "Beata"
city1 = "Gdynia"
first_name2 = "Michał"
city2 = "Toruń"

welcome1 = f"Witaj {first_name1}. \nMiło Cię widzieć w naszym mieście: {city1}! "
print(welcome1)

welcome2 = f"Witaj {first_name2}. \nMiło Cię widzieć w naszym mieście: {city2}!"
print(welcome2)

print(f"Wynik mnożenia 6 przez 3 to {6*3}")

## Algebra ###
circle_radius = 4
area_of_a_circle_with_radius_5 = 3.14 * circle_radius ** 2
print(f"Area of  a circle with radius {circle_radius}:", area_of_a_circle_with_radius_5)
circumference_of_a_circle_with_radius_5 = 2 * 3.14 * circle_radius
print(f"Circumference of  a circle with radius {circle_radius}", circumference_of_a_circle_with_radius_5)

long_mathematical_expression = 2 + 3 + 5 * 7 + 4 / 3 * (3 + 5 / 2) + 7**2 - 13
print(long_mathematical_expression)

def print_welcome_message(name, city):
    print(f"Witaj {name.upper()}, miło Cię widzieć w naszym mieście {city.upper()}.")

print_welcome_message ("Beata", "Gdynia")
print_welcome_message ("Michał", "Toruń")
print_welcome_message ("adam", "kraków")


def print_email_adress(name, last_name):
    return f"{name.lower()}{last_name.lower()}@4testers.com"

print(print_email_adress("Janusz", "Nowak"))
print(print_email_adress("Barbara", "Kowalska"))
