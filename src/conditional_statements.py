def print_word_length_description(name):
    if len(name) > 5:
        print(f'The name {name} is longer than 5 characters')
    else:
        print(f" The {name} is 5 characters or shorter")


if __name__ == '__main__':
   short_name = "Ada"
   long_name = "Adrian"

print_word_length_description(long_name)
print_word_length_description(short_name)

def weather_condition(celsius, hectopascals):
    if celsius == 0 and hectopascals ==1013:
        return True
    else:
        return False

print(weather_condition(0, 1013))
print(weather_condition(1, 1014))
print(weather_condition(0, 1014))
print(weather_condition(1, 1014))

def get_grade_for_exam_points(number_of_points):
    if number_of_points >= 90:
        return 5
    elif number_of_points >=75:
        return 4
    elif number_of_points >=50:
        return 3
    else:
        return False
print(get_grade_for_exam_points(91))
print(get_grade_for_exam_points(90))
print(get_grade_for_exam_points(89))
print(get_grade_for_exam_points(76))
print(get_grade_for_exam_points(75))
print(get_grade_for_exam_points(74))
print(get_grade_for_exam_points(51))
print(get_grade_for_exam_points(50))
print(get_grade_for_exam_points(49))

def person_is_a_adult(age):
    if age >= 18:
        return True
    else:
        return False

age_Zofia =  11
age_adam = 13
age_staszek =  18

print("Zofia", person_is_a_adult(age_Zofia))
print("Adam", person_is_a_adult(age_adam))
print("Staszek", person_is_a_adult(age_staszek))




