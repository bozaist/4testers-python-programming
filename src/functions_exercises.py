def upper_word(word):
    return word.upper()


big_dog = upper_word("dog")
print(big_dog)
print(upper_word("tree"))


def calculate_area_of_a_circle(radius):
    return 3.1415 * radius ** 2


area_of_a_circle_with_radius_10 = calculate_area_of_a_circle(10)
print(area_of_a_circle_with_radius_10)


def calculate_volume_of_a_cuboid(a, b, c):
    return a * b * c

print("volume of a cuboid is:", calculate_volume_of_a_cuboid(5, 6, 7))


def calculate_value_square_of_a_number(a):
    return a ** 2


value_square_of_a_number = calculate_value_square_of_a_number(0)
print("Kwadrat cyfry 0 to", value_square_of_a_number)

value_square_of_a_number = calculate_value_square_of_a_number(16)
print("Kwadrat liczby 16 to", value_square_of_a_number)

value_square_of_a_number = calculate_value_square_of_a_number(2.55)
print("Kwadrat liczby 2.55 to", value_square_of_a_number)


def convert_celcius_to_farenheit(celcius):
    return 9 / 5 * celcius + 32


print(convert_celcius_to_farenheit(20))


def email(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"


print(email("Janusz", "Nowak"))
print(email("Barbara", "Kowalska"))


def print_given_number_multiplied_by_3(input_number):
    print(input_number * 3)


print_given_number_multiplied_by_3(40)


def calculate_area_of_a_triangle(a, h):
    return (0.5 * a * h)

area_of_a_triangle = calculate_area_of_a_triangle(5, 10)
print(area_of_a_triangle)

def calculate_value_squer_of_number(a):
    return a**2

print(f"Kwadrat cyfry 3 to:{calculate_value_squer_of_number(3)}")

def convert_celsius_to_farhenheit(temp_in_celsius):
    return 9/5 * temp_in_celsius +  32

print(convert_celsius_to_farhenheit(20))
