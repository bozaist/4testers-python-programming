

# Description  of my friend
friend_name = "Justyna"
friend_age = 43
number_of_pets = 1
friendship_time = 20.5
is_driver = True

print(friend_name)
print(friend_age)
print(number_of_pets)
print(friendship_time)
print(is_driver)
print(friend_name, friend_age, number_of_pets, friendship_time, is_driver, sep="\t")

friend = {
    "name": "Justyna",
    "age": 50,
    "hobby": ["books", "cycling"]
}
print(friend)
print(friend["age"])

friend["city"] = "Wroclaw"
print(friend)
print(friend["hobby"][-1])
friend["hobby"].append("swimming")
print(friend["hobby"])
print(len(friend["hobby"]))


def sum_of_the_numbers(list_of_the_numbers):
    return sum(list_of_the_numbers)

january = [-4, 1.0, -7, 2]
february = [-13, -9, -3, 3]

print(f'January:{sum_of_the_numbers(january)}')
print(f'February:{sum_of_the_numbers(february)}')


def average_of_numbers(a,b):
    return (a+b)/2
a = sum_of_the_numbers(january)
b = sum_of_the_numbers(february)

print(average_of_numbers(a,b))




