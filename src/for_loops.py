def print_each_student_name_capitalized(list_of_student_first_names):
    for first_name in list_of_student_first_names:
        print(first_name.capitalize())

def print_first_ten_integers_squared():
    for integer in range(1, 11):
        print(integer ** 2)

import uuid
def print_10_random_uuids(number_of_strings):
    for i in range(number_of_strings):
        print(str(uuid.uuid4()))


def print_each_temperatures_in_both_scales(list_of_temps_in_celsius):
    for temp in list_of_temps_in_celsius:
        temp_fahrenheit = temp * 9/5 + 32
        print(f"Celsius: {temp}, Fahrenheit: {temp_fahrenheit}")




if __name__ == '__main__':

    list_of_students = ["kate", "marek", "tosia", "nicki", "stephane"]
    print_each_student_name_capitalized(list_of_students)
    print_first_ten_integers_squared()
    print_10_random_uuids(8)

    temp_celsius = [10.3, 23.4, 15.8, 19, 14, 23]
    print_each_temperatures_in_both_scales(temp_celsius)
