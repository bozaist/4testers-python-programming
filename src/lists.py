movies = ["Star Wars", "Harry Potter", "Władca Pierścieni", "Pulp Fiction", "Forest Gump"]
first_movie = movies[0]
print(f"First movie is  {first_movie}.")

print(f"Last movie is {movies[-1]}.")

movies.append("Dune")
print(f"the length of movies list is {len(movies)}")

movies.insert(0, "Marsjanin")
movies.remove("Władca Pierścieni")
print(movies)

movies[-1] = "Dune (1980)"
print(movies)

emails = ["a@example.com", "b@example.com"]
print(f"Dlugosc  listy emails to:{len(emails)}")
print(f"Pierwszy element listy emails to:{emails[0]}")
print(f"Ostatni element listy emails to:{emails[1]}")
emails.append("cde@example.com")
print(emails)

my_children = ["Staszek", "Adam", "Zosia"]
print(my_children[-2])
my_children.append("Joanna")
print(my_children)
number_of_my_children = len(my_children)
print(number_of_my_children)

first_three_of_my_children = my_children[0:3]
print(first_three_of_my_children)

#dictionary

friend ={
    "name": "Justyna",
    "age": 43,
    "hobby": ["reading", "cycling"]
}
print(friend["age"])
friend["city"] = "Wrocław"
friend["age"] = "39"
print(friend)
print(friend["hobby"][-1])
friend["hobby"].append("climbing")
print(friend)



