def get_country_of_a_car_brand(car_brand):
    if car_brand in ("Toyota", "Mazda", "Suzuki", "Subaru"):
        return "Japan"
    elif car_brand in ("BMW", "Mercedes", "Audi", "Volkswagen"):
        return "Germany"
    elif car_brand in ("Renault", "Peugeot"):
        return "France"
    else:
        return "Unknown"


